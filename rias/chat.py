import datetime as dt
import json
import os

import pymongo
import redis

mongo_host = os.getenv("MONGO_HOST", "localhost")
client = pymongo.MongoClient(f"mongodb://mongodb:password123@{mongo_host}", 27017)
REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
redis = redis.Redis(REDIS_HOST)


class ChatManager:
    chat_db = client.chat
    messages_collection = chat_db.messages
    CACHE_KEY = 'CHAT_MESSAGES'

    @classmethod
    def get_messages(cls):

        raw_messages = redis.get(cls.CACHE_KEY)
        if raw_messages is None:
            print('cache is missing')
            messages = [{
                'username': x['username'],
                'message': x['message'],
                'time': str(x['time'])
            } for x in cls.messages_collection.find()]
            redis.set(cls.CACHE_KEY, json.dumps(messages), ex=60)
        else:
            print('found in cache')
            messages = json.loads(raw_messages)
        return messages

    @classmethod
    def drop_cache(cls):
        redis.delete(cls.CACHE_KEY)

    @classmethod
    def add_message(cls, user_name: str, message: str):
        cls.messages_collection.insert_one(
            {'username': user_name,
             'message': message,
             'time': dt.datetime.now()})
        cls.drop_cache()

    @classmethod
    def delete_all_messages(cls):
        cls.messages_collection.drop()
        cls.drop_cache()