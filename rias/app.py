import json
import os
from functools import lru_cache

import pika
from flask import Flask, request

RMQ_HOST = os.getenv('RMQ_HOST', 'localhost')

app = Flask(__name__)
from chat import ChatManager


@lru_cache()
def get_rmq_connection():
    connection = pika.BlockingConnection(pika.ConnectionParameters(RMQ_HOST, port=5672))
    channel = connection.channel()
    channel.queue_declare(queue='q_message')
    channel.close()
    return connection


@app.route("/get_chat/", methods=['GET'])
def get_chat():
    return {'messages': ChatManager.get_messages()}


@app.route("/send_message/", methods=['POST'])
def send_message():
    data = request.json
    message_json = json.dumps(data)

    channel = get_rmq_connection().channel()
    channel.queue_declare(queue='q_message')

    channel.basic_publish(exchange='',
                          routing_key='q_message',
                          body=message_json.encode())

    return {'sucess': True}


def read_rmq():
    connection = pika.BlockingConnection(pika.ConnectionParameters(RMQ_HOST, port=5672))
    channel = connection.channel()
    channel.queue_declare(queue='q_message')
    channel.basic_consume('q_message', on_message)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    except Exception as e:
        channel.stop_consuming()
        print(e)


def on_message(channel, method_frame, header_frame, body):
    print(body)
    data = json.loads(body)
    username = data['name']
    message = data['message']
    ChatManager.add_message(username, message)
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)
