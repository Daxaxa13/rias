"""Проверка приложения под нагрузкой"""

import datetime as dt
from concurrent.futures.thread import ThreadPoolExecutor

import requests

from chat import ChatManager


def add_message(idx):
    requests.post('http://localhost:5000/send_message/',
                  json={'name': f"NAME {idx}", "message": f"Hello {idx}"})


if __name__ == '__main__':
    ChatManager.delete_all_messages()
    t1 = dt.datetime.now()
    with ThreadPoolExecutor(max_workers=100) as executor:
        for i in range(1000):
            executor.submit(add_message, i)
    t2 = dt.datetime.now()
    print(f"Затраченное время {t2 - t1}")
