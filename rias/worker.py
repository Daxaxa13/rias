from concurrent.futures.thread import ThreadPoolExecutor
from time import sleep

from app import read_rmq


def run_worker(idx):
    print(f"worker {idx} started")
    while True:
        try:
            read_rmq()
        except Exception as e:
            sleep(10)


if __name__ == '__main__':

    with ThreadPoolExecutor(max_workers=10) as executor:
        for i in range(10):
            executor.submit(run_worker, i)
